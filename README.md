# open-tt-guides
This is a collection of guides that are related to *Triple Trouble*, a world boss event in the *Guild Wars 2* video game.

You can find a rendered version of the guides [here](https://kingdread.de/tt-guides). This repository contains the LaTeX source that is used to render the HTML and PDF versions.

## Compiling
The repository comes with a `Makefile` that has rules for PDF generation (using `pdflatex`) and HTML generation (using `tex4ht`/`make4ht`).

1. Go to the subfolder of the guide that you want to compile
2. Run `make pdf` to generate the PDF in the `build/` folder or `make html` to generate the HTML files in the `html/` folder
3. You can use `make minify` to minify the PDF after compilation.

Of course, you can use any LaTeX processing software to modify the source or generate other formats as well.

## License
This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).

Content obtained from Guild Wars 2, its web sites, manuals and guides, concept art and renderings, press and fansite kits, and other such copyrighted material, may also be used in this document. All rights, title and interest in and to such content remains with ArenaNet or NCsoft, as applicable, and such content is not licensed pursuant to CC-BY-SA.
