all: pdf html


pdf:
	mkdir -p build
	pdflatex -shell-escape -output-directory build $(NAME).tex
	pdflatex -shell-escape -output-directory build $(NAME).tex

minify:
	ps2pdf -dPrinted=false build/$(NAME).pdf build/$(NAME).min.pdf

html:
	mkdir -p html

ifeq ($(findstring htmlconf.cfg,$(wildcard *.cfg)),)
	$(eval HTMLCONF=)
else
	$(eval HTMLCONF=--config htmlconf)
endif
	make4ht -s -d html $(HTMLCONF) $(NAME).tex
	make4ht -s -d html $(HTMLCONF) $(NAME).tex
	cat ../custom.css >> html/$(NAME).css
	rm $(NAME).4ct $(NAME).4tc $(NAME).aux $(NAME).dvi $(NAME).idv $(NAME).lg $(NAME).log $(NAME).tmp $(NAME).xref $(NAME).css $(NAME)*.html


.PHONY: all pdf html
