\documentclass[a4paper]{article}

\input{../preamble.tex}
\usepackage{float}

% Metadata
\title{Triple Trouble Husk Handling Guide}
\author{Peter Parker IV \& others}
\date{2019-08-06 \color{gray}r0}

\hypersetup{
pdftitle={Triple Trouble Husk Handling Guide},
pdfsubject={},
pdfauthor={Peter Parker IV},
pdfkeywords={Triple Trouble, Wurm, Guild Wars 2}
}

\begin{document}

\maketitle

\input{../legals.tex}

\begin{abstract}
  This guide should give you an introduction of how husk handling at Triple Trouble works.

  Husk handling is one of the special roles needed to have a smooth run.
  It refers to the task of dealing with the \emph{Champion Partially Digested Husk} that the wurm spits out, which requires condition damage and a bit of crowd control.

  Husk handling is easy to learn and does not require a lot of practise, as long as you have the right gear ready.
\end{abstract}

\newpage
\tableofcontents
\newpage

\section{Introduction}

Husk handling is one of the special roles at Triple Trouble.
It refers to the task of taking care of the \href{https://wiki.guildwars2.com/wiki/Champion_Partially_Digested_Husk}{\emph{Champion Partially Digested Husks}} (called \emph{husks} for short) that the wurm can spit out every 90 seconds.
This is usually done by bringing enough (condition) damage so that you can kill a husk on your own, while also bringing some form of \href{https://wiki.guildwars2.com/wiki/Control_effect}{\emph{Crowd Control}} (CC) in order to break the husk's \href{https://wiki.guildwars2.com/wiki/Defiance_bar}{\emph{Defiance Bar}} and stun it.

The reason why husks deserve their own special team is because of the specific challenge that they pose to the event.
First of all, husks have a very high \href{https://wiki.guildwars2.com/wiki/Toughness}{toughness}.
This means that they are pretty much immune to any sort of direct, power-based damage.
As such, just hitting them with your weapon will generally result in very low damage numbers, even if you run a fully offensive gear like Berserker's (Power, Precision, Ferocity).

Therefore, the only way do kill husks is to use \href{https://wiki.guildwars2.com/wiki/Condition}{\emph{conditions}}, as their damage will not be reduced by toughness.
This is why huskies are sometimes referred to as \emph{condis} as well.
Having dedicated people to take care of the husks ensures that they do have the required condition damage to deal with them.

The second reason why husks are dangerous is because they can knock players away from the stack, which can interfere with the mechanics, especially at the Amber or Cobalt wurm head.
They can also inflict players with \href{https://wiki.guildwars2.com/wiki/Parasites}{Parasites}, which will stun them periodically.

In summary, stray husks in the arena can make the event harder by interfering with the wurm head mechanic, stunning players, preventing them from dealing damage during the burn phase or even attacking the egg blocker.

On the bright side, husk handling is a very easy special role to execute, and a good start for beginners that want to help out more with the event.
It doesn't rely on recognizing animations or pressing skills in the right order.
Instead, the only thing that you need to do is to bring some condition damage and some CC - and in return you will get to whack some husks!
Additionally, even a \enquote{bad} husk handler is useful - just keeping the husk busy for a bit can already ensure that the squad has enough time to finish their mechanic, after which they can come and help to kill the husk.

Each husk spit will produce three husks.
Therefore, each head should have at least three husk handlers to take care of them - one husk handler per husk.
Of course, you can also have multiple people dealing with a single husk.


\section{Gear}

Since husks will only take damage from conditions, your gear should be selected in a way that makes conditions more powerful.
The stats Power, Precision and Ferocity only affect direct damage and as such will be wasted.
Instead, players who focus on conditions can improve their damage by taking gear with either \href{https://wiki.guildwars2.com/wiki/Condition_Damage}{\emph{Condition Damage}} or \href{https://wiki.guildwars2.com/wiki/Expertise}{\emph{Expertise}} as attributes.

\emph{Condition Damage} will directly affect how much damage each tick of your condition does.
As such, having a high condition damage value is important for a husk handler.
Additionally, gear with Condition Damage as an attribute is readily available in the form of the Sinister, Carrion, Rabid or Dire prefixes - even off the Trading Post.

\emph{Expertise} is the other attribute that affects your damage.
Instead of increasing the damage per tick, it instead increases the duration of the conditions that you apply.
Expertise is a bit harder to get by, and as such doesn't need to be a priority for husk handlers.
The fight against the husk will only take a few seconds anyway, so having too much Expertise will be unnecessary.

If you have a character with \emph{Viper's} gear from e.g. raiding, then you can use that.
It offers enough Condition Damage and Expertise to comfortably handle husks.
On the other hand, if you are just starting out, crafting Viper's gear only for husk handling is not necessary.
You can stick with one of the cheaper options, especially the ones that are not account bound, so that you can buy them from the Trading Post.

Keep in mind that your stats will be scaled down for the event, as Bloodtide Coast is not a level 80 area.
A commonly used \enquote{breakpoint} is around 350 Condition Damage - if you are above that while standing at \emph{Firthside Vigil Waypoint}, you should be fine!


\section{Skills \& Traits}

As a husk handler, you usually aim to kill your husk.
Remember that you can only damage it by using conditions, so any skills that inflict conditions and any traits that either increase your condition damage or your condition duration are useful.
Which ones exactly those are depends on the specific profession that you are playing, but generally speaking, every profession has some way of being played as a condition damage dealer.

In addition to pure damage, you also want to bring some \emph{Crowd Control} abilities.
Those are any abilities that knock back, stun, daze, pull, \textellipsis your enemy.
The husks have a defiance bar, which means you can not affect their movement directly.
Instead, every use of a CC ability will lower their defiance bar, and once the bar is depleted, the husk will be stunned.
This can help you to keep the husk in place and stop it from running towards the zerg, giving you enough time to apply conditions and to kill it.

If you want, you can also bring a skill that gives yourself \href{https://wiki.guildwars2.com/wiki/Stability}{Stability}.
This will prevent the husk from knocking you up or away.

As for your traits, there are no special requirements.
Take anything that increases your condition damage, that prolongs your condition duration or that applies extra conditions.


\section{Husk Landing Spots}

Each wurm head has three predefined spots at which the husks will be landing.
As a husk handler, you usually don't want to run along with your commander.
Instead, you wait at your husk landing spot for your husk so that you can immediately start damaging it.

Keep in mind that you do not want to stand right on top of the spot - if you stand on the spot while a husk is landing, it will down you.
Take a step to the side to ensure that you will survive, or mount up if you're out of combat so that your mount will take the hit.

In the following pictures, the first husk landing spot is marked with the green Arrow marker, the second one with the purple Circle marker and the third one with the red Heart marker.


\subsection{Amber}

From the point of the wurm, the husk landing spots are roughly in the directions of the three trees that you can see.
The Arrow and Circle husks are the closest ones to the swallowing spot, which makes them a bit more dangerous than the husk at the Heart spot.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{images/spots-overview-amber.jpg}
  \caption{Husk landing spots on Amber.}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{images/spots-minimap-amber.png}
  \caption{Husk landing spots on Amber (minimap).}
\end{figure}


\subsection{Cobalt}

On Cobalt, each keg run path has a husk on the way.
As such, the Arrow husk will be landing in the direction of the mast spot, the Circle husk will be landing towards the Jumping Puzzle and the Heart husk will be landing towards the Beach.

If you want to make sure that you don't get hit by the husk, you can climb the rock formation between the second and the third landing spot.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{images/spots-overview-cobalt.jpg}
  \caption{Husk landing spots on Cobalt.}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{images/spots-minimap-cobalt.png}
  \caption{Husk landing spots on Cobalt (minimap).}
\end{figure}


\subsection{Crimson}

The anchoring points at Crimson are the blue extractor (east on the map) and the red extractor (north on the map).
The first husk will be landing between the wurm and the blue extractor, while the third husk will be landing towards the red extractor.
As for the second husk, it will be landing in the valley between the two extractors, a bit farther behind.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{images/spots-overview-crimson.jpg}
  \caption{Husk landing spots on Crimson.}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.5\textwidth]{images/spots-minimap-crimson.png}
  \caption{Husk landing spots on Crimson (minimap).}
\end{figure}


\section{Husk Blocking}

It is possible to block the husks instead of letting them spawn.
This technique is usually called \enquote{husk blocking} (or \enquote{fullblocking} when combined with egg blocking).
However, not every egg blocker is able to block husks, which is why having husk handlers can still be useful.

If there are fullblockers on your wurm, make sure to coordinate with them about being a husk handler.
Otherwise you might not get to see any husks.
If you want to learn husk blocking yourself, check out the Eggblocking guide, as it also covers husk blocking.


\section{Epilogue}

Husk handling is easy to get into and a lot of fun.
If you want to get a bit more into the event, and you want to take on a bit more responsibility, then this is the perfect way to get started.
Later on, if you feel like it, you can then learn about egg-/fullblocking or even commanding - or you just stay as a zergling or husk handler.

It might seem like husk handlers are not needed nowadays, as a lot of runs are done with a fullblocker.
However, that is not strictly true.
For one thing, fullblocking is harder and requires more practise than eggblocking, so not every eggblocker can do it.
Secondly, sometimes husk blockers also miss something, in which case having a back up husky can be useful.

Most importantly though, husk handling is fun.
If you enjoy doing it, then the eggblocker can lean back a bit more and just provide you with a husk.
After all, we're doing the event because we like it!

As such, learning how to husk handle can still be worthwhile, both for the event and for your personal enjoyment.

\end{document}
